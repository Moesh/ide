# Moesh's Ideal Development Environment

This set of commands creates an ideal development environment for making Minecraft custom maps.

Diary for this project: http://moesh.ca/topics/dev-diary/ide/

Introduces run, end, loop, initLoop, break, continue commands to allow function-oriented style programming.

Moesh's IDE is written to work with Gnasp's Smelt (https://github.com/GnaspGames/Smelt).

# Goals

* Create perscriptive methods to keep Minecraft Command Code maintainable by multiple people
* Produce tools to ease team-oriented development

# Naming Conventions

After a few weeks of switching between different naming conventions based on what language I was pulling from, I've decided to standarize my naming conventions for EVERYTHING I do from now on. This list is by no means definitive, but it may give some guidance on how to structure your own work.

## Filenames
*./folder_name/lowercased_underscored.php*

This includes resource packs, folders, structures, textures, models, and sounds.

## Functions, States
*PascalCase*

Functions are usually names of AECs, States are categorities, essentially. Covers function/marker names, boolean tags, and types/groups.

## Variables
*camalCase*

## Constants
*ALL_CAPS_AND_UNDERSCORED*

Still nouns.

Constants are held under the const default scoreboard. Use these when you are running scoreboard operations. These are values which will never change, for example:

'''/scoreboard players set 20 const 20
/scoreboard players set 1 const 1
/scoreboard players set MAX_POWER const 9000'''